<!-- PASTILLAS -->
<?php
    //session_start();
    //require_once("conn/conn.php");
    //$sql = "UPDATE usuario SET nombre=".$nombre.", apellido=".$apellido.", username=".$usuario.", email=".$email." WHERE id=".$idusuario;
    //mysql_query($sql, $mysqli);
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <link rel="icon" type="image/png" href="img/icon.png" />
                <!-- CSS -->
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
                <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
                
                <title>Datos Personales</title>
            </head>

            <header>
            	<?php include_once('menu.php'); ?>
            </header>

            <body>
            <br>
                <section id="contact-form-section" class="form-content-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="tab-content">
                                <div class="col-sm-12">
                                    <div class="item-wrap">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="item-content colBottomMargin">
                                                    <div class="item-info">
                                                        <h2 class="item-title text-center">Actualizar Datos</h2>
                                                    </div>
                                            </div>
                                            </div>
                                            <div class="col-md-12">
                                            <form id="contactForm" name="contactform" data-toggle="validator" class="popup-form">
                                                <div class="row">
                                                    <div id="msgContactSubmit" class="hidden"></div>  
                                                        <div class="form-group col-sm-6">
                                                            <div class="help-block with-errors"></div>
                                                                <input name="fname" id="fname" placeholder="Tu nombre y apellido" class="form-control" type="text" required data-error="Por favor ingresa tu nombre y apellidos"> 
                                                                <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <div class="help-block with-errors"></div>
                                                                <input name="email" id="email" placeholder="Tu E-mail*" pattern=".*@\w{2,}\.\w{2,}" class="form-control" type="email" required data-error="Por favor ingresa un correo electrónico válido">
                                                                <div class="input-group-icon"><i class="fa fa-envelope"></i></div>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <div class="help-block with-errors"></div>
                                                                <input name="phone" id="phone" placeholder="Teléfono*" class="form-control" type="text" required data-error="Por favor ingresa tu número de teléfono">
                                                                <div class="input-group-icon"><i class="fa fa-phone"></i></div> 
                                                            </div>
                                                            <div class="form-group last col-sm-12">
                                                                <button type="submit" id="submit" class="btn btn-custom"><i class='fa fa-envelope'></i> Actualizar</button>
                                                            </div>
                                                            <span class="sub-text">* Campos requeridos</span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        <div class="colBottomMargin">
            &nbsp;<div class="colBottomMargin">&nbsp;</div>
        </div>	
        <div id="footer" class="footer">
            <div class="container">			
                <div class="row">					
                    <div class="footer-top col-sm-12">
                        <p class="text-center copyright">&copy; 2021 - Ivet Garcia </p>
                    </div> 
                </div>		
            </div>
        </div>
        <a href="#" class="scrollup"><i class="fa fa-arrow-circle-up"></i></a>
        
        <!-- JS -->
        <!-- jQuery Library -->
        <script src="js/jquery-3.2.1.min.js"></script>	
        <!-- Popper js -->
        <script src="js/popper.min.js"></script>
        <!-- Bootstrap Js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Form Validator -->
        <script src="js/validator.min.js"></script>
        <!-- Contact Form Js -->
        <script src="js/form.js"></script>
        </body>
        </html>