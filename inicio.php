<!-- INICIO -->
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <link rel="icon" type="image/png" href="img/icon.png" />
                <!-- CSS -->
                <link rel="stylesheet" href="css/inicio.css">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
                <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

                
                <title>Inicio</title>
            </head>

            <header>
            	<?php include_once('menu.php'); ?>
            </header>
            
            <body>
                <h2>Bienvenido, <?php echo $_SESSION['SESS_NAME'];?> ! </h2>
                <a href="pastillas.php"><div class="pastillas">
                	<nav class="pastillas-nav">
                    	<p>PASTILLAS</p><br />
                        <i style="font-size:150px;" class="fa fa-capsules"></i>
                    </nav>
                </div></a>
                <a href="reloj.php"><div class="alarma">
                	<nav class="alarma-nav">
                    	<p>ALARMA</p><br />
                        <i style="font-size:150px;" class="fas fa-bell"></i>
                    </nav>
                </div></a>
                <a href="calendario.php"><div class="calendario">
                	<nav class="calendario-nav">
                    	<p>CALENDARIO</p><br />
                        <i style="font-size:150px;" class="fa fa-calendar-alt"></i>
                    </nav>
                </div></a>
                <a href="chat.php"><div class="chat">
                	<nav class="chat-nav">
                    	<p>CHAT</p><br />
                        <i style="font-size:150px;" class="fa fa-comment-dots"></i>
                    </nav>
                </div></a>
                <a href="datos.php"><div class="datos">
                	<nav class="datos-nav">
                    	<p>DATOS PERSONALES</p><br />
                        <i style="font-size:50px;" class="fa fa-user-cog"></i>
                    </nav>
                </div></a>
            </body>
		</html>