<!-- MENU -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <link rel="icon" type="image/png" href="img/icon.png" />
                <!-- CSS -->
                <link rel="stylesheet" href="css/menu.css">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
                <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

                
                <title>Inicio</title>
            </head>
            <header>
            	<nav class="izq">
                	<ul class="menu">
                    	<li><a href="inicio.php">Inicio</a></li>
                        <li><a href="pastillas.php">Pastillas</a></li>
                        <li><a href="reloj.php">Alarma</a></li>
                        <li><a href="calendario.php">Calendario</a></li>
                        <li><a href="chat.php">Chat</a></li>
                    </ul>
                    <div class="hide">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                   	</div>
                </nav>
                <nav>
                	<ul class="menu">
                    	<li><a href="logout.php">Cerrar Sesion</a></li>
                        <li><a href="datos.php">Datos personales</a></li>
                    </ul>
                </nav>
            </header>
            
            <!-- SCRIPT PARA HACER EL MENÚ -->        
            <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
            <script>
                $(".hide").on('click', function() {
                    $("nav ul").toggle('slow');
                })
            </script>
        </html>