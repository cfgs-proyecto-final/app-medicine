<!-- RELOJ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <link rel="icon" type="image/png" href="img/icon.png" />
                <!-- CSS -->
                <link rel="stylesheet" href="css/reloj.css">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
                <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
                
                <title>Reloj</title>
            </head>

            <header>
            	<?php include_once('menu.php'); ?>
            </header>

            <body onload="startTime()">
                <div id="clockdate">
                    <div class="clockdate-wrapper">
                        <div id="clock"></div>
                        <div id="date"></div>
                    </div>
                </div>
                
                <script src="js/reloj.js"></script>
            </body>
        </html>